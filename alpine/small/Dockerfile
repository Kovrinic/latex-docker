FROM conoria/alpine-pandoc:latest as pandocsrc
FROM ruby:2.6-alpine

# markup format conversion tool
COPY --from=pandocsrc usr/bin/pandoc usr/bin/pandoc
COPY --from=pandocsrc usr/bin/pandoc-citeproc usr/bin/pandoc-citeproc

# setup latex user
ARG USERNAME=latex
ARG USERHOME=/home/latex
ARG USERID=1000
ARG USERGECOS=LaTEX

RUN adduser \
  --home "$USERHOME" \
  --uid $USERID \
  --gecos "$USERGECOS" \
  --disabled-password \
  "$USERNAME"

# copy texlive install profile to tmp
COPY texlive-small.profile /tmp/

# main install
RUN apk update \
  && apk --no-cache add \
  # some auxiliary tools
  wget git make perl tar xz \
  # ruby gemfile dependent
  libc-dev g++ \  
  # py-pygments: Required for syntax highlighting using minted.
  perl python py-pygments \  
  # ghostscript: Required for ImageMagick to convert PDF to Image
  ghostscript \
  # imagemagick: Required for converting images
  imagemagick \
  # install latest texlive
  && wget "http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz" \
  && tar -xzf install-tl-unx.tar.gz \
  && cd install-tl-* \
  && ./install-tl -no-gui -profile /tmp/texlive-small.profile \
  && cd ../ \
  # cleanup install
  && rm -rf install-tl-* \
  && rm -rf /tmp/texlive-small.profile

# path recommendation: https://github.com/bundler/bundler/pull/6469#issuecomment-383235438
ENV PATH /opt/texlive/bin/x86_64-linuxmusl:$PATH

# install texliveonfly & latexmk
RUN tlmgr install texliveonfly latexmk
