# ruby-latex

Dockerfiles for building LaTeX documents and creating static webpages with Jekyll.
Built from [ruby:2.5][r25] and added LaTeX support.  Optional [Alpine Linux][al] support to help reduce size and build time.

## Supported tags and respective `Dockerfile` links

- **[Debian Stretch + Ruby 2.5][r25]**
    - [small][sdf] _(minimal + latex, xetex, metapost, and a few languages)_
    - [full][fdf] _(everything)_
- **[Alpine 3.9 + Ruby 2.6][ra26]**
    - [small-alpine][sadf] _(minimal + latex, xetex, metapost, and a few languages)_
    - [full-alpine][fadf] _(everything)_

## Image Variants

### ruby-latex:small

Latest _([small scheme][tlss])_ [TeX Live][tl] installation bundled additionally with [texliveonfly][tlof] & [latexmk][lxmk].  Suggest using [texliveonfly][tlof] first to auto-install required Tex packages, then run [latexmk][lxmk] to update bookmarks.

##### Example:

```bash
#!/bin/bash

# vars
RESUME_BASE_NAME="Resume_MatthewRothfuss"

# create PDF for resume site, resume.cls requires XeLaTeX
# run twice, 1st to gather requried tex files, 2nd to generate bookmarks
texliveonfly --compiler=xelatex --terminal_only -a "-pdf -jobname=${RESUME_BASE_NAME} -interaction=nonstopmode" resume.tex
latexmk -pdf -jobname="${RESUME_BASE_NAME}" -e '$pdflatex=q/xelatex %O -interaction=nonstopmode %S/' resume.tex
```

Also includes support for converting PDFs to images, using [ImageMagick][im] and [Ghostscript][gs].

##### PDF to Image Example:

```bash
# convert PDFs into png images for Readme
convert -trim -density 150 -antialias "${RESUME_BASE_NAME}.pdf" -background white -resize 1024x -quality 100 -flatten -sharpen 0x1.0 "${RESUME_BASE_NAME}_FA5.png"
```

### ruby-latex:full

Latest complete _([full scheme][tlfs])_ [TeX Live][tl] installation.  Includes PDF to image support, using [ImageMagick][im] and [Ghostscript][gs].

##### PDF to Image Example:

```bash
#!/bin/bash

# vars
RESUME_BASE_NAME="Resume_MatthewRothfuss"

# convert PDFs into png images for Readme
convert -trim -density 150 -antialias "${RESUME_BASE_NAME}.pdf" -background white -resize 1024x -quality 100 -flatten -sharpen 0x1.0 "${RESUME_BASE_NAME}_FA5.png"
```

---
- _Repository hosted [on Bitbucket][bb]._
- _Containers hosted [on Docker][rt]._


[bb]: https://bitbucket.org/Kovrinic/latex-docker
[sdf]:https://bitbucket.org/Kovrinic/latex-docker/src/master/stretch/small/Dockerfile
[fdf]:https://bitbucket.org/Kovrinic/latex-docker/src/master/stretch/full/Dockerfile
[sadf]:https://bitbucket.org/Kovrinic/latex-docker/src/master/alpine/small/Dockerfile
[fadf]:https://bitbucket.org/Kovrinic/latex-docker/src/master/alpine/full/Dockerfile
[r25]:https://github.com/docker-library/ruby/blob/master/2.5/stretch/Dockerfile
[ra26]:https://github.com/docker-library/ruby/blob/master/2.6/alpine3.9/Dockerfile
[tlof]:https://ctan.org/pkg/texliveonfly
[lxmk]:https://ctan.org/pkg/latexmk
[tlss]:https://www.tug.org/texlive/doc/texlive-en/texlive-en.html#x1-25010r5
[tlfs]:https://www.tug.org/texlive/doc/texlive-en/texlive-en.html#x1-25006r3
[im]:https://www.imagemagick.org
[gs]:https://www.ghostscript.com/
[tl]:https://www.tug.org/texlive/
[rt]:https://hub.docker.com/r/kovrinic/ruby-latex/
[al]:https://alpinelinux.org/
